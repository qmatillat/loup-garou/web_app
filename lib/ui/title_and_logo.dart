import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:loupgarou/ui/blendlogo.dart';

const _imgWidth = 300;

class TitleAndLogo extends StatelessWidget {
  const TitleAndLogo({
    required this.title,
    required this.content,
    this.contentAlignment = Alignment.topCenter,
    this.bg,
    Key? key,
  }) : super(key: key);

  final Widget title;
  final Widget content;
  final Alignment contentAlignment;

  final Color? bg;

  @override
  Widget build(BuildContext context) => Container(
        color: bg,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Logo(),
            const SizedBox(height: 50),

            title,

            const SizedBox(height: 5),

            // Fixed size container to avoid change in size when reconnecting
            Container(
              height: 100,
              alignment: contentAlignment,
              child: content,
            ),
          ],
        ),
      );
}

class Logo extends StatelessWidget {
  const Logo({
    this.width = _imgWidth,
    Key? key,
  }) : super(key: key);

  final int width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width.toDouble(),
      child: BlendingLogo(
        bg: Image.asset(
          "images/wolf.png",
          cacheWidth: width,
        ),
        wireframe: Image.asset(
          "images/wolf_wireframe.png",
          cacheWidth: width,
        ),
      ),
    );
  }
}
