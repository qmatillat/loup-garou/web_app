import 'dart:math' as math;

import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';

const disableAnimation = kDebugMode;

class BlendingLogo extends StatefulWidget {
  const BlendingLogo({
    required this.bg,
    required this.wireframe,
    this.duration = const Duration(seconds: 3),
  });

  final Duration duration;
  final Widget bg;
  final Widget wireframe;

  @override
  _BlendingLogoState createState() => _BlendingLogoState();
}

class _BlendingLogoState extends State<BlendingLogo>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: widget.duration,
    vsync: this,
  )..repeat();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => disableAnimation
      ? widget.bg
      : AnimatedBuilder(
          animation: _controller,
          child: widget.bg,
          builder: (context, child) => Stack(
            fit: StackFit.passthrough,
            children: [
              child!,
              ShaderMask(
                blendMode: BlendMode.dstIn,
                shaderCallback: (b) =>
                    buildLinearGradient(b, _controller.value),
                child: widget.wireframe,
              ),
            ],
          ),
        );

  Shader buildLinearGradient(Rect bounds, double value) => LinearGradient(
        transform: _GradientTranslate(value * 3 - 1),
        begin: Alignment.topLeft,
        colors: const [
          Colors.transparent,
          Colors.white,
          Colors.white,
          Colors.transparent,
        ],
        stops: const [0, 0.2, 0.3, 0.4],
      ).createShader(bounds);
}

class _GradientTranslate extends GradientTransform {
  const _GradientTranslate(this.dist);

  final double dist;

  @override
  Matrix4 transform(Rect bounds, {TextDirection? textDirection}) {
    final max = math.max(bounds.width, bounds.height);
    return Matrix4.identity()..translate(dist * max);
  }
}
