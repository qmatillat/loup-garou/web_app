import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  Input({
    required this.onSubmit,
    required this.labelText,
    this.icon = const Icon(Icons.send),
    String? defaultValue,
    Key? key,
  })  : nameController = TextEditingController(text: defaultValue),
        super(key: key);

  final ValueChanged<String> onSubmit;
  final String? labelText;
  final Icon icon;

  final TextEditingController nameController;

  static final _theme = ThemeData.from(
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.grey,
      brightness: Brightness.dark,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: _theme,
      child: TextField(
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          filled: true,
          labelText: labelText,
          suffixIcon: IconButton(
            onPressed: () => onSubmit(nameController.text),
            icon: icon,
          ),
        ),
        controller: nameController,
        textInputAction: TextInputAction.done,
        onSubmitted: onSubmit,
      ),
    );
  }
}
