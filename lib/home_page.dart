import 'package:flutter/material.dart';

import 'package:loupgarou/log.dart';
import 'package:loupgarou/root_msg.dart';

import 'package:loupgarou/websocket_client/widgets.dart';
import 'package:loupgarou/ws_state/in_room.dart';
import 'package:loupgarou/ws_state/loading_screen.dart';
import 'package:loupgarou/ws_state/running_game.dart';
import 'package:loupgarou/ws_state/state_list.dart';
import 'package:loupgarou/ws_state/upgraded.dart';
import 'package:loupgarou/ws_state/with_name.dart';

class HomePageContext extends InheritedWidget {
  const HomePageContext({
    Widget? child,
    required this.backendUrl,
    this.logPrefix,
    this.userName,
    this.autologin = false,
    this.roomName,
    Key? key,
  }) : super(child: child ?? const HomePage(), key: key);

  final Uri backendUrl;
  final String? logPrefix;
  final String? userName;
  final String? roomName;
  final bool autologin;

  @override
  bool updateShouldNotify(HomePageContext old) =>
      backendUrl != old.backendUrl ||
      logPrefix != old.logPrefix ||
      userName != old.userName ||
      roomName != old.roomName ||
      autologin != old.autologin;

  static HomePageContext of(BuildContext context) {
    final widget =
        context.dependOnInheritedWidgetOfExactType<HomePageContext>();
    if (widget != null) return widget;

    throw FlutterError.fromParts(<DiagnosticsNode>[
      ErrorSummary(
        'HomePageContext.of() called with an empty context.',
      ),
      context.describeElement('The context used was'),
    ]);
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final key = GlobalKey<WebSocketClientState>();

  @override
  Widget build(BuildContext context) => WebSocketClientWidget(
        uri: HomePageContext.of(context).backendUrl,
        connecting: Connecting.new,
        jsonHandler: _wsHandleText,
        key: key,
      );

  bool _wsHandleText(dynamic msg, WebSocketClientState state) {
    if (msg is! Map<String, dynamic>) return false;

    final res = Response.fromJson(msg.cast());

    if (res.currStep != null) {
      final step = res.currStep!;
      logger.i("New step: $step");

      const stateList = StateList([
        UpgradedInfo(),
        WithNameInfo(),
        InRoomInfo(),
        RunningGameInfo(),
      ]);

      state.setCurrWidget(stateList[step]);
      return true;
    }

    if (res.error != null) {
      logger.e("Websocket returned an error: ${res.error}");

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text("Error received"),
          content: Text(res.error!),
          actions: [
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: const Text("OK"),
            )
          ],
          backgroundColor: Theme.of(context).errorColor,
        ),
      );
      return true;
    }

    return false;
  }
}
