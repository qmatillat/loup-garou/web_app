import 'package:loupgarou/websocket_client/interface.dart';

class Response {
  Response.fromJson(Map<String, String> obj)
      : currStep = obj["CurrStep"],
        error = obj["Error"];

  final String? currStep;
  final String? error;
}

class SpecialCommand extends Serialize {
  SpecialCommand.stepInfo() : stepInfo = true;

  bool stepInfo = false;

  @override
  Map<String, dynamic> toJson() {
    if (stepInfo) return {"StepInfo": null};

    return {};
  }
}
