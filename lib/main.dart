import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:loupgarou/home_page.dart';

void main() {
  runApp(MyApp());
}

const multiApp = kDebugMode;
const rawUri = "ws://localhost:8888";

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Loup Garou',
      theme: ThemeData.from(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.grey,
          backgroundColor: Colors.deepPurple.shade900,
          brightness: Brightness.dark,
          cardColor: Colors.deepPurple.shade600,
        ),
      ),
      home: multiApp
          ? const MultiHome()
          : HomePageContext(backendUrl: Uri.parse(rawUri)),
    );
  }
}

class MultiHome extends StatelessWidget {
  const MultiHome({this.nbElement = 4, Key? key}) : super(key: key);

  final int nbElement;

  Iterable<Widget> _childList(Widget Function(int i) item) sync* {
    for (int i = 0; i < nbElement - 1; i++) {
      yield Expanded(child: item(i));
      yield const VerticalDivider();
    }
    yield Expanded(child: item(nbElement - 1));
  }

  @override
  Widget build(BuildContext context) => Row(
        children: _childList(
          (i) => HomePageContext(
            backendUrl: Uri.parse(rawUri),
            userName: "User $i",
            autologin: true,
            roomName: i == 1 ? "MyRoom" : null,
          ),
        ).toList(),
      );
}
