import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:loupgarou/log.dart';
import 'package:loupgarou/ui/title_and_logo.dart';
import 'package:loupgarou/websocket_client/interface.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

typedef LoadingWidget = Widget Function({VoidCallback? retry, Key? key});
typedef OnConnected = void Function(WebSocketClientState state);
typedef JsonHandler = bool Function(dynamic msg, WebSocketClientState state);

class WebSocketClientWidget extends StatefulWidget {
  const WebSocketClientWidget({
    required this.uri,
    required this.connecting,
    required this.jsonHandler,
    Key? key,
  }) : super(key: key);

  final Uri uri;
  final LoadingWidget connecting;

  final JsonHandler jsonHandler;

  @override
  WebSocketClientState createState() => WebSocketClientState();
}

class WebSocketClientState extends State<WebSocketClientWidget> {
  @override
  void initState() {
    super.initState();

    _connect();
  }

  @override
  void dispose() {
    _ws?.sink.close(WebSocketStatus.normalClosure, "WebSocketWidget disposed");

    super.dispose();
  }

  Uri get uri => widget.uri;

  var _key = GlobalKey<WebSocketState>();
  WebSocketWidget? currWidget;

  WebSocketChannel? _ws;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("LoupGarou"),
          leading: currWidget?.leadingWidget,
          actions: [
            PopupMenuButton(
              itemBuilder: (context) => [
                const PopupMenuItem(
                  child: AboutListTile(
                    applicationName: "Loup-Garou",
                    applicationVersion: "0.1.0",
                    applicationIcon: Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      child: Logo(width: 250),
                    ),
                  ),
                  // ),
                ),
              ],
            )
          ],
        ),
        body: currWidget ??
            widget.connecting(
              retry: _ws == null ? _connect : null,
              key: _key,
            ),
      );

  void _handleMessage(String msg) {
    logger.v("Message received : $msg");

    try {
      final decMsg = json.decode(msg);

      final handled = widget.jsonHandler(decMsg, this);
      if (!handled) {
        if (_key.currentState == null) {
          // Need to wait for the widget to be renderer and key to be updated
          WidgetsBinding.instance!
              .addPostFrameCallback((ts) => {_handleMessage(msg)});
        } else {
          _key.currentState!.onRawMessage(decMsg);
        }
      }
    } catch (e, s) {
      logger.e("Unable to decode json.\nValue $msg", e, s);
    }
  }

  Future<void> _connect() async {
    if (_ws != null) {
      logger.w("Ask to reconnect, but a connection already exist");
      return;
    }

    logger.i("Connecting websocket to $uri");

    final ws = WebSocketChannel.connect(uri);
    setState(() => _ws = ws);

    try {
      await ws.stream.forEach((e) => _handleMessage(e as String));
    } catch (e, s) {
      logger.e("Exception received while reading from ws", e, s);
      ws.sink.close(WebSocketStatus.abnormalClosure);
    }

    logger.v("WebSocket over");
    // Stopping might have been called by dispose, so dont set state in this case
    if (mounted) {
      setState(() {
        _ws = null;
        currWidget = null;
      });
    }
  }

  void setCurrWidget(WebSocketInfo bldr) {
    setState(() {
      _key = GlobalKey(debugLabel: "StateWidget: ${bldr.name}");
      currWidget = bldr.createWidget(_ws!.sink, key: _key);
    });
  }
}
