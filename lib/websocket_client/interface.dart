import 'dart:convert';

import 'package:flutter/widgets.dart';

abstract class Serialize {
  Object? toJson();
}

class Void implements Serialize {
  Void.fromJson(dynamic _);

  @override
  Object? toJson() => null;
}

abstract class WebSocketInfo {
  const WebSocketInfo();

  abstract final String name;

  WebSocketWidget createWidget(Sink sink, {Key? key});
}

abstract class WebSocketWidget extends StatefulWidget {
  @override
  const WebSocketWidget(Sink sink, {Key? key})
      : _msgSender = sink,
        super(key: key);

  final Sink _msgSender;

  @override
  WebSocketState createState();

  Widget? get leadingWidget => null;

  void sendMessage<O extends Serialize>(O msg) {
    _msgSender.add(json.encode(msg.toJson()));
  }
}

typedef FromJsonFunc<In> = In Function(dynamic src);

abstract class WebSocketState<In, Out extends Serialize,
    Root extends WebSocketWidget> extends State<Root> {
  WebSocketState(this._fromJson);

  void onMessage(In msg);

  void sendMessage(Out msg) => widget.sendMessage(msg);

  final FromJsonFunc<In> _fromJson;
  final VoidCallback? backPressed = null;

  void onRawMessage(dynamic msg) => onMessage(_fromJson(msg));
}

abstract class VoidWebSocketState<T extends WebSocketWidget>
    extends WebSocketState<Void, Void, T> {
  VoidWebSocketState() : super(Void.fromJson);

  @override
  void onMessage(Void msg) {}
}
