import 'package:flutter/material.dart';
import 'package:loupgarou/websocket_client/interface.dart';

class StateList {
  const StateList(this.items);

  final List<WebSocketInfo> items;

  WebSocketInfo operator [](String name) => items.firstWhere(
        (e) => e.name == name,
        orElse: () => _StateNotFoundInfo(name, names),
      );

  List<String> get names => items.map((e) => e.name).toList();
}

class _StateNotFoundInfo extends WebSocketInfo {
  _StateNotFoundInfo(this.stateName, this.knownState);

  @override
  WebSocketWidget createWidget(Sink sink, {Key? key}) =>
      _StateNotFound(stateName, knownState, sink, key: key);

  @override
  String get name => "_StateNotFound<$name>";

  final String stateName;
  final List<String> knownState;
}

class _StateNotFound extends WebSocketWidget {
  const _StateNotFound(this.stateName, this.knownState, Sink sink, {Key? key})
      : super(sink, key: key);

  final String stateName;
  final List<String> knownState;

  @override
  _StateNotFoundState createState() => _StateNotFoundState();
}

class _StateNotFoundState extends VoidWebSocketState<_StateNotFound> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.orange,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Internal error",
            textAlign: TextAlign.center,
            style: Theme.of(this.context).textTheme.headline4,
          ),
          Text(
            "Reached invalid state <${widget.stateName}>",
            textAlign: TextAlign.center,
            style: Theme.of(this.context).textTheme.headline6,
          ),

          // Spacer
          const SizedBox(height: 25),

          Text(
            "Known state:\n${widget.knownState}",
            textAlign: TextAlign.center,
            style: Theme.of(this.context).textTheme.bodyText1,
          ),

          // Cheat for full width
          Flex(direction: Axis.horizontal),
        ],
      ),
    );
  }
}
