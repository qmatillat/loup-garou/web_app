import 'package:flutter/material.dart';
import 'package:loupgarou/ui/title_and_logo.dart';

class Connecting extends StatelessWidget {
  const Connecting({this.retry, Key? key}) : super(key: key);

  final VoidCallback? retry;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return TitleAndLogo(
      bg: retry == null ? null : theme.errorColor,
      title: Text(
        retry == null ? "Connecting to server" : "Connection to server failed",
        style: theme.textTheme.headline4?.apply(color: Colors.white),
      ),
      content: retry == null
          ? SizedBox(
              width: 310,
              child: LinearProgressIndicator(
                color: theme.colorScheme.secondary,
                backgroundColor: theme.primaryColorDark,
              ),
            )
          : ElevatedButton.icon(
              onPressed: retry,
              icon: const Icon(Icons.refresh),
              label: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Retry",
                  style: theme.textTheme.headline5,
                ),
              ),
            ),
    );
  }
}
