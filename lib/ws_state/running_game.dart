import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:loupgarou/log.dart';
import 'package:loupgarou/websocket_client/interface.dart';
import 'package:loupgarou/ws_state/game/cupidon.dart';
import 'package:loupgarou/ws_state/game/display_role.dart';
import 'package:loupgarou/ws_state/game/meta/game_state.dart';
import 'package:loupgarou/ws_state/game/meta/game_state_list.dart';
import 'package:loupgarou/ws_state/game/meta/message.dart';
import 'package:loupgarou/ws_state/game/not_your_turn.dart';

class RunningGameInfo extends WebSocketInfo {
  const RunningGameInfo();

  @override
  String get name => "RunningGame";

  @override
  _RunningGame createWidget(Sink socket, {Key? key}) =>
      _RunningGame(socket, key: key);
}

class _RunningGame extends WebSocketWidget {
  const _RunningGame(Sink socket, {Key? key}) : super(socket, key: key);

  @override
  RunningGameState createState() => RunningGameState();
}

class RunningGameState
    extends WebSocketState<RunningGameInput, RunningGameOutput, _RunningGame>
    implements MessageSender {
  RunningGameState()
      : super((e) => RunningGameInput.fromJson(e as Map<String, dynamic>)) {
    logger.i("Begin handling state RunningGame");
  }

  bool initialDisplayRole = true;
  String? myRoleId;

  TurnInfo? tinfo;

  Widget? w;

  GlobalKey<GameStatefullState> childKey =
      GlobalKey(debugLabel: "RunningGame_load");

  @override
  Widget build(BuildContext context) => initialDisplayRole
      ? DisplayRole(
          myRoleId,
          () => setState(() {
            initialDisplayRole = false;
          }),
        )
      : w ?? const NotYourTurn();

  @override
  void onMessage(RunningGameInput msg) {
    if (msg.roleInfo != null) {
      setState(() {
        myRoleId = msg.roleInfo;
      });
    }

    if (msg.turnInfo != null) {
      setTurnInfo(msg.turnInfo!);
    }

    if (childKey.currentState == null) {
      WidgetsBinding.instance!.addPostFrameCallback((ts) => {onMessage(msg)});
    } else {
      childKey.currentState!.onMessage(msg);
    }
  }

  void setTurnInfo(TurnInfo turnInfo) {
    setState(() {
      tinfo = turnInfo;
      childKey = GlobalKey(debugLabel: "RunningGame_${turnInfo.name}");
      w = null;
      if (turnInfo.isPlaying) {
        const lst = GameSateList([
          CupidonInfo(),
        ]);

        w = lst[turnInfo.name].createWidget(this, key: childKey);
      }
    });
  }
}
