import 'package:flutter/material.dart';

import 'package:loupgarou/home_page.dart';
import 'package:loupgarou/ui/input.dart';
import 'package:loupgarou/websocket_client/interface.dart';

class WithNameOutput extends Serialize {
  WithNameOutput.roomList()
      : roomList = true,
        createName = null,
        joinId = null;
  WithNameOutput.createRoom(String name)
      : roomList = false,
        createName = name,
        joinId = null;
  WithNameOutput.join(dynamic id)
      : roomList = false,
        createName = null,
        joinId = id;

  final bool roomList;
  final dynamic joinId;
  final String? createName;

  @override
  Object toJson() {
    if (roomList) return {"RoomList": null};
    if (joinId != null) return {"Join": joinId};
    if (createName != null) {
      return {
        "CreateRoom": {"name": createName}
      };
    }

    return {};
  }
}

class RoomInfo {
  RoomInfo.fromJson(Map<String, dynamic> obj)
      : id = obj["id"],
        name = obj["name"] as String,
        numPlayer = obj["num_player"] as int;

  final dynamic id;
  final String name;
  final int numPlayer;
}

class WithNameInput {
  WithNameInput.fromJson(Map<String, dynamic> obj)
      : roomList = (obj["RoomList"] as List<dynamic>?)
            ?.map((value) => RoomInfo.fromJson(value as Map<String, dynamic>))
            .toList(),
        ok = obj.containsKey("Ok"),
        roomNotFound = obj.containsKey("RoomNotFound");

  final List<RoomInfo>? roomList;
  final bool ok;
  final bool roomNotFound;
}

class WithNameInfo extends WebSocketInfo {
  const WithNameInfo();

  @override
  String get name => "WithName";

  @override
  _WithName createWidget(Sink socket, {Key? key}) =>
      _WithName(socket, key: key);
}

class _WithName extends WebSocketWidget {
  const _WithName(Sink socket, {Key? key}) : super(socket, key: key);

  @override
  _WithNameState createState() => _WithNameState();
}

class _WithNameState
    extends WebSocketState<WithNameInput, WithNameOutput, _WithName> {
  _WithNameState()
      : super((e) => WithNameInput.fromJson(e as Map<String, dynamic>));

  @override
  void initState() {
    listRoom();
    super.initState();
  }

  List<RoomInfo> roomList = [];

  @override
  Widget build(BuildContext context) {
    final ctx = HomePageContext.of(context);
    if (ctx.roomName != null) {
      createRoom(ctx.roomName!);
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 32.0),
      child: Column(
        children: [
          // Create
          Text(
            "Create a new room",
            style: Theme.of(context).textTheme.headline4,
          ),
          const SizedBox(height: 32.0),
          Input(
            labelText: "New room name",
            icon: const Icon(Icons.add),
            onSubmit: createRoom,
          ),
          const SizedBox(height: 64.0),

          // Or join
          Text(
            "Or choose a room to join",
            style: Theme.of(context).textTheme.headline4,
          ),
          TextButton(
            onPressed: listRoom,
            child: const Text("Reload list room !"),
          ),
          SizedBox(
            height: 250,
            child: ListView.separated(
              itemBuilder: (context, index) => _renderItem(roomList[index]),
              separatorBuilder: (context, index) => const Divider(
                color: Colors.grey,
                indent: 200,
                endIndent: 200,
              ),
              itemCount: roomList.length,
            ),
          )
        ],
      ),
    );
  }

  void listRoom() => sendMessage(WithNameOutput.roomList());
  void join(dynamic id) => sendMessage(WithNameOutput.join(id));
  void createRoom(String name) => sendMessage(WithNameOutput.createRoom(name));

  @override
  void onMessage(WithNameInput msg) {
    if (msg.roomList != null) {
      setState(() => roomList = msg.roomList!);
    }
  }

  Widget _renderItem(RoomInfo room) => ListTile(
        onTap: () => join(room.id),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(room.name),
            const SizedBox(width: 25),
            Text(
              "(${room.numPlayer} players)",
              style: Theme.of(context).textTheme.caption,
            ),
          ],
        ),
        trailing: IconButton(
          onPressed: () => join(room.id),
          icon: const Icon(Icons.chevron_right),
        ),
      );
}
