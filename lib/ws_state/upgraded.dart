import 'package:flutter/material.dart';

import 'package:loupgarou/home_page.dart';
import 'package:loupgarou/log.dart';
import 'package:loupgarou/ui/input.dart';
import 'package:loupgarou/ui/title_and_logo.dart';
import 'package:loupgarou/websocket_client/interface.dart';

class _UpgradedOutput extends Serialize {
  _UpgradedOutput({this.nickname, this.rejoin});

  String? nickname;
  int? rejoin;

  @override
  Object toJson() {
    if (nickname != null) return {"Nickname": nickname};
    if (rejoin != null) return {"Rejoin": rejoin};

    return {};
  }
}

enum _UpgradedInput {
  ok,
  roomNotFound,
}

_UpgradedInput fromJson(String s) {
  if (s == "Ok") return _UpgradedInput.ok;
  if (s == "RoomNotFound") return _UpgradedInput.roomNotFound;

  throw Exception("Invalid _UpgradedInput <$s>");
}

class UpgradedInfo extends WebSocketInfo {
  const UpgradedInfo();

  @override
  String get name => "Upgraded";

  @override
  _Upgraded createWidget(Sink socket, {Key? key}) =>
      _Upgraded(socket, key: key);
}

class _Upgraded extends WebSocketWidget {
  const _Upgraded(Sink socket, {Key? key}) : super(socket, key: key);

  @override
  _UpgradedState createState() => _UpgradedState();
}

class _UpgradedState
    extends WebSocketState<_UpgradedInput, _UpgradedOutput, _Upgraded> {
  _UpgradedState() : super((_) => _UpgradedInput.ok) {
    logger.i("Begin handling state Upgraded");
  }

  @override
  Widget build(BuildContext context) {
    final ctx = HomePageContext.of(context);
    final username = ctx.userName;
    if (ctx.autologin && username != null) {
      logger.i("Autologin with username $username");
      login(username);
    }

    return TitleAndLogo(
      title: Text(
        "Welcome",
        style: Theme.of(context).textTheme.headline4,
      ),
      content: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: Input(
          labelText: "Choose your nickname",
          onSubmit: login,
          defaultValue: username,
        ),
      ),
    );
  }

  void login(String name) {
    logger.i("Logging in with name : $name");
    sendMessage(_UpgradedOutput(nickname: name));
  }

  void rejoin() {}

  @override
  void onMessage(_UpgradedInput msg) {
    // Nothing special here, if ok, should automatically transfer to new state
  }
}
