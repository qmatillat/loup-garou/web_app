import 'package:flutter/material.dart';

class NotYourTurn extends StatelessWidget {
  const NotYourTurn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.black54,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "This is not your turn",
                style: Theme.of(context).textTheme.headline3,
              ),
              Text(
                "Have a nice sleep",
                style: Theme.of(context).textTheme.headline6,
              ),
            ],
          ),
        ),
      );
}
