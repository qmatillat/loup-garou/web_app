import 'package:flutter/material.dart';
import 'package:loupgarou/log.dart';
import 'package:loupgarou/ws_state/game/meta/game_state.dart';
import 'package:loupgarou/ws_state/game/meta/message.dart';

class GameSateList {
  const GameSateList(this.items);

  final List<GameStateInfo> items;

  GameStateInfo operator [](String name) => items.firstWhere(
        (e) => e.name == name,
        orElse: () => _StateNotFoundInfo(name, names),
      );

  List<String> get names => items.map((e) => e.name).toList();
}

class _StateNotFoundInfo extends GameStateInfo {
  _StateNotFoundInfo(this.stateName, this.knownState);

  @override
  GameStatefullWidget createWidget(MessageSender m, {Key? key}) =>
      _StateNotFound(stateName, knownState, m, key: key);

  @override
  String get name => "_StateNotFound<$name>";

  final String stateName;
  final List<String> knownState;
}

class _StateNotFound extends GameStatefullWidget {
  const _StateNotFound(
    this.stateName,
    this.knownState,
    MessageSender m, {
    Key? key,
  }) : super(m, key: key);

  final String stateName;
  final List<String> knownState;

  @override
  _StateNotFoundState createState() => _StateNotFoundState();
}

class _StateNotFoundState extends GameStatefullState<_StateNotFound> {
  @override
  void onMessage(RunningGameInput i) {
    logger.w("StateNotFound received message, message is lost forever");
  }

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.orange,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Internal error",
              textAlign: TextAlign.center,
              style: Theme.of(this.context).textTheme.headline4,
            ),
            Text(
              "Reached invalid state <${widget.stateName}>",
              textAlign: TextAlign.center,
              style: Theme.of(this.context).textTheme.headline6,
            ),

            // Spacer
            const SizedBox(height: 25),

            Text(
              "Known state:\n${widget.knownState}",
              textAlign: TextAlign.center,
              style: Theme.of(this.context).textTheme.bodyText1,
            ),

            // Cheat for full width
            Flex(direction: Axis.horizontal),
          ],
        ),
      );
}
