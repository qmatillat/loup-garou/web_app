import 'package:flutter/widgets.dart';

import 'package:loupgarou/ws_state/game/meta/message.dart';

abstract class GameStateInfo {
  const GameStateInfo();

  abstract final String name;

  GameStatefullWidget createWidget(MessageSender m, {Key? key});
}

abstract class MessageSender {
  void sendMessage(RunningGameOutput msg);
}

abstract class GameStatefullWidget extends StatefulWidget {
  const GameStatefullWidget(this._msgSender, {Key? key}) : super(key: key);

  final MessageSender _msgSender;
}

abstract class GameStatefullState<Super extends GameStatefullWidget>
    extends State<Super> {
  void onMessage(RunningGameInput i);

  void _sendMessage(RunningGameOutput m) => widget._msgSender.sendMessage(m);

  void sendPlayerList() => _sendMessage(RunningGameOutput.playerList());

  void sendVoteDone({bool voteDone = true}) =>
      _sendMessage(RunningGameOutput.voteDone(voteDone: voteDone));

  void sendVoteFor({int? playerId, int kind = 0}) =>
      _sendMessage(RunningGameOutput.voteFor(kind: kind, playerId: playerId));
}
