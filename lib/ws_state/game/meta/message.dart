import 'package:flutter/material.dart';
import 'package:loupgarou/websocket_client/interface.dart';

@immutable
class RunningGameOutput extends Serialize {
  RunningGameOutput.voteFor({required this.kind, this.playerId})
      : playerList = false,
        voteDone = null;
  RunningGameOutput.voteDone({required this.voteDone})
      : playerList = false,
        kind = null,
        playerId = null;
  RunningGameOutput.playerList()
      : playerList = true,
        voteDone = null,
        kind = null,
        playerId = null;

  final bool playerList;

  final bool? voteDone;

  // VoteFor
  final int? kind;
  final int? playerId;

  @override
  Object toJson() {
    if (playerList) return "PlayerList";
    if (voteDone != null) return {"VoteDone": voteDone};
    if (kind != null) {
      return {
        "VoteFor": {"kind": kind, "player_id": playerId}
      };
    }

    return {};
  }
}

@immutable
class TurnInfo {
  const TurnInfo({
    required this.name,
    required this.num,
    required this.isPlaying,
  });

  static TurnInfo? fromJson(List<dynamic>? l) => l != null
      ? TurnInfo(
          name: l[0] as String,
          num: l[1] as int,
          isPlaying: l[2] as bool,
        )
      : null;

  final String name;
  final int num;
  final bool isPlaying;
}

@immutable
class Player {
  Player.fromJson(Map<String, dynamic> object)
      : id = object["id"] as int,
        name = object["name"] as String,
        status = object["status"] as String;

  final int id;
  final String name;
  final String status;
}

@immutable
class RunningGameInput {
  RunningGameInput.fromJson(Map<String, dynamic> json)
      : players = (json["PlayerList"] as List<dynamic>?)
            ?.map((e) => Player.fromJson(e as Map<String, dynamic>))
            .toList(),
        roleInfo = json["RoleInfo"] as String?,
        turnInfo = TurnInfo.fromJson(json["TurnInfo"] as List<dynamic>?),
        selected = (json["Selected"] as List<dynamic>?)
            ?.map((e) => (e as List<dynamic>).map((e) => e as int).toList())
            .toList();

  final String? roleInfo;
  final TurnInfo? turnInfo;
  final List<Player>? players;
  final List<List<int>>? selected;
}
