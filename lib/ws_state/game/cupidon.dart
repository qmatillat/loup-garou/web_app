import 'package:flutter/material.dart';
import 'package:loupgarou/log.dart';
import 'package:loupgarou/ws_state/game/meta/game_state.dart';
import 'package:loupgarou/ws_state/game/meta/message.dart';

class CupidonInfo extends GameStateInfo {
  const CupidonInfo();

  @override
  String get name => "Cupidon";

  @override
  GameStatefullWidget createWidget(MessageSender m, {Key? key}) =>
      _Cupidon(m, key: key);
}

class _Cupidon extends GameStatefullWidget {
  const _Cupidon(MessageSender m, {Key? key}) : super(m, key: key);

  @override
  State<StatefulWidget> createState() => _CupidonState();
}

class _CupidonState extends GameStatefullState<_Cupidon> {
  @override
  void initState() {
    super.initState();
    sendPlayerList();
  }

  List<Player>? players;
  bool kind = false;
  int get nextKind => kind ? 1 : 0;
  set nextKind(int v) => kind = v == 1;

  // Map playerId to voted kind
  Map<int, int> selectedId = {};

  @override
  void onMessage(RunningGameInput i) {
    logger.v("Cupidon Message received ${i.toString()}");
    if (i.players != null) {
      setState(() {
        players = i.players;
      });
    }

    if (i.selected != null) {
      setState(() {
        selectedId = {for (var e in i.selected!) e[0]: e[1]};
      });
    }
  }

  @override
  Widget build(BuildContext context) => Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text("Cupidon", style: Theme.of(context).textTheme.headline1),
              const SizedBox(height: 40),
              Text(
                "Selects 2 players to become a Love Couple",
                style: Theme.of(context).textTheme.headline5,
              ),
              const SizedBox(height: 15),
              Text(
                "These players are now loyal to each other.\nIf one of these players dies, the other will die as well.",
                style: Theme.of(context).textTheme.headline6,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 40),
              Expanded(
                child: ListView.builder(
                  itemCount: players?.length ?? 0,
                  itemBuilder: (ctx, id) {
                    final p = players![id];
                    final selKind = selectedId[p.id];

                    return ListTile(
                      title: Text(p.name),
                      trailing: Icon(
                        selKind == null
                            ? Icons.favorite_border_outlined
                            : Icons.favorite,
                      ),
                      tileColor: selKind != null
                          ? selKind == nextKind
                              ? Colors.pinkAccent.withAlpha(200)
                              : Colors.pinkAccent
                          : null,
                      onTap: () {
                        if (selKind != null) {
                          sendVoteFor(kind: selKind);
                          nextKind = selKind;
                        } else {
                          sendVoteFor(kind: nextKind, playerId: p.id);

                          kind = !kind;
                        }
                      },
                    );
                  },
                ),
              ),
              ElevatedButton(
                onPressed: selectedId.length == 2 ? sendVoteDone : null,
                child: const Text("Ready"),
              )
            ],
          ),
        ),
      );
}
