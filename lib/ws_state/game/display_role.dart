import 'package:flutter/material.dart';

class DisplayRole extends StatelessWidget {
  const DisplayRole(this.roleId, this.ok, {Key? key}) : super(key: key);

  final VoidCallback ok;
  final String? roleId;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Your role is ", style: Theme.of(context).textTheme.headline3),
          Text(roleId ?? "...", style: Theme.of(context).textTheme.headline2),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: ok,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "OK !",
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
