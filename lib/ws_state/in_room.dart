import 'package:flutter/material.dart';
import 'package:loupgarou/websocket_client/interface.dart';

class InRoomOutput extends Serialize {
  InRoomOutput.playerList()
      : playerList = true,
        setReady = null,
        leaveRoom = false;
  InRoomOutput.setReady({bool ready = true})
      : playerList = false,
        setReady = ready,
        leaveRoom = false;
  InRoomOutput.leaveRoom()
      : playerList = false,
        setReady = null,
        leaveRoom = true;

  final bool playerList;
  final bool? setReady;
  final bool leaveRoom;

  @override
  Object toJson() {
    if (playerList) return {"PlayerList": null};
    if (leaveRoom) return {"LeaveRoom": null};
    return {
      "SetReady": setReady,
    };
  }
}

class _Player {
  _Player.fromJson(Map<String, dynamic> val)
      : id = val["id"],
        name = val["name"] as String,
        isReady = val["status"] == "Ready",
        isConnected = val["status"] != "Disconnected";

  dynamic id;
  String name;

  bool isReady;
  bool isConnected;
}

class InRoomInput {
  InRoomInput.fromJson(Map<String, dynamic> obj)
      : players = (obj["PlayerList"] as List<dynamic>)
            .map((e) => _Player.fromJson(e as Map<String, dynamic>))
            .toList();

  List<_Player> players;
}

class InRoomInfo extends WebSocketInfo {
  const InRoomInfo();

  @override
  String get name => "InRoom";

  @override
  _InRoom createWidget(Sink socket, {Key? key}) => _InRoom(socket, key: key);
}

class _InRoom extends WebSocketWidget {
  const _InRoom(Sink socket, {Key? key}) : super(socket, key: key);

  @override
  _InRoomState createState() => _InRoomState();

  @override
  Widget get leadingWidget => BackButton(
        onPressed: () => sendMessage(InRoomOutput.leaveRoom()),
      );
}

class _InRoomState extends WebSocketState<InRoomInput, InRoomOutput, _InRoom> {
  _InRoomState()
      : super((e) => InRoomInput.fromJson(e as Map<String, dynamic>));

  List<_Player> players = [];
  bool ready = false;

  @override
  void initState() {
    listRoom();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 32.0),
        child: Column(
          children: [
            SwitchListTile(
              title: const Text("Ready"),
              value: ready,
              onChanged: (r) => setReady(ready: r),
            ),
            SizedBox(
              height: 250,
              child: ListView.separated(
                itemBuilder: (context, index) => _renderItem(players[index]),
                separatorBuilder: (_, __) => const Divider(color: Colors.grey),
                itemCount: players.length,
              ),
            )
          ],
        ),
      );

  void listRoom() => sendMessage(InRoomOutput.playerList());
  void leaveRoom() => sendMessage(InRoomOutput.leaveRoom());
  void setReady({bool ready = true}) {
    sendMessage(InRoomOutput.setReady(ready: ready));
    setState(() {
      this.ready = ready;
    });
  }

  @override
  void onMessage(InRoomInput msg) {
    setState(() => players = msg.players);
  }

  Widget _renderItem(_Player player) {
    return ListTile(
      enabled: player.isConnected,
      title: Text(player.name),
      trailing: Icon(
        player.isConnected
            ? (player.isReady ? Icons.check : Icons.hourglass_empty)
            : Icons.download,
      ),
    );
  }
}
