import 'dart:convert';

import 'package:logger/logger.dart';

const _prettyLog = false;

final logger = Logger(
  printer: _prettyLog
      ? PrettyPrinter(
          printTime: true,
          lineLength: 90,
        )
      : CustomPrinter(),
);

class CustomPrinter extends LogPrinter {
  static final levelPrefixes = {
    Level.verbose: 'VERBOSE',
    Level.debug: '  DEBUG',
    Level.info: '   INFO',
    Level.warning: 'WARNING',
    Level.error: '  ERROR',
    Level.wtf: '    WTF',
  };

  static final levelColors = {
    Level.verbose: AnsiColor.fg(AnsiColor.grey(0.5)),
    Level.debug: AnsiColor.none(),
    Level.info: AnsiColor.fg(12),
    Level.warning: AnsiColor.fg(208),
    Level.error: AnsiColor.fg(196),
    Level.wtf: AnsiColor.fg(199),
  };

  @override
  List<String> log(LogEvent event) {
    final messageStr = _stringifyMessage(event.message);
    final errorStr = event.error != null ? '  ERROR: ${event.error}' : '';

    String toDigit(num val, num digit) {
      var ret = val.toString();
      while (ret.length < digit) {
        ret = '0$ret';
      }
      return ret;
    }

    final now = DateTime.now();
    final h = toDigit(now.hour, 2);
    final m = toDigit(now.minute, 2);
    final s = toDigit(now.second, 2);
    final milli = toDigit(now.millisecond, 3);
    final date = "$h:$m:$s.$milli";

    // final func = StackTrace.current.toString().split('\n')[3].split(' ').last;
    final chunk = StackTrace.current
        .toString()
        .split('\n')
        .where(
          (line) =>
              !line.contains("dart-sdk") &&
              !line.contains("packages/logger") &&
              !line.contains("package:logger") &&
              !line.contains("/log.dart"),
        )
        .first
        .split(' ');

    // final func = chunk.last;
    final func = chunk.first[0] == '#'
        ? chunk.last // Native
        : "${chunk[0].replaceFirst("packages/", "package:")}:${chunk[1]}"; // Web

    return ['$date ${_labelFor(event.level)} $messageStr$errorStr\t$func'];
  }

  String _labelFor(Level level) {
    final prefix = levelPrefixes[level]!;
    final color = levelColors[level]!;

    return color(prefix);
  }

  String _stringifyMessage(dynamic message) {
    if (message is Map || message is Iterable) {
      const encoder = JsonEncoder.withIndent(null);
      return encoder.convert(message);
    } else {
      return message.toString();
    }
  }
}
